function addActiveHeader() {
    let btn = document.querySelector('.header__dropdown-btn');
    let menu = document.querySelector('.header__dropdown-menu');
    btn.addEventListener('click', e => {
       if (e.target.closest('.header__dropdown-btn')) {
          btn.classList.toggle('active');
          menu.classList.toggle('active');
          Array.from(menu.children).forEach(i => i.classList.remove('active'));
          menu.addEventListener('click', e => {
             if (e.target.closest('header__drop-menu-item')) {
                Array.from(menu.children).forEach(i => i.classList.remove('active'));
                e.target.classList.add('active');
             }
          });
       };
    });
 };
 addActiveHeader();